# FlightChainUI

The flight chain UI is a simple demo webapp that allows users to search by flight key, and it will display
the history of changes to a flight.

This project assumes that there is a [REST API interface](../flight-chain-api) running on `localhost:3000` - this can be set in the  `src/environments/environment.ts` file.

## Deployment

The whole Flight Chain system runs in a k8s service. This angular app is built into a docker image (with nginx), and this docker image is bundled as a container in a pod 
with the Flight Chain API. 


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also us e `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

